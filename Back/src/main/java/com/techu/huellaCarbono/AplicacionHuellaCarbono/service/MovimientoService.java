package com.techu.huellaCarbono.AplicacionHuellaCarbono.service;

import com.techu.huellaCarbono.AplicacionHuellaCarbono.model.MovimientoModel;
import com.techu.huellaCarbono.AplicacionHuellaCarbono.repository.MovimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MovimientoService {

    @Autowired
    MovimientoRepository movimientoRepository;

    //Read All
    public List<MovimientoModel> findAll(){
        return movimientoRepository.findAll();
    }

    //Read by id
    public Optional<MovimientoModel> findById(Integer id){
        return movimientoRepository.findById(id);
    }

    //Save
    public MovimientoModel save(MovimientoModel movimiento){
        if(movimiento.getCo2() == 0) {
            if (movimiento.getImporte() != 0) {
                if (movimiento.getCategoria().equalsIgnoreCase("viaje")) {
                    movimiento.setCo2((int) (movimiento.getImporte() * 100)); //añadir parámetro categoria
                } else if (movimiento.getCategoria().equalsIgnoreCase("comercio")) {
                    movimiento.setCo2((int) (movimiento.getImporte() * 25)); //añadir parámetro categoria
                } else if (movimiento.getCategoria().equalsIgnoreCase("transporte")) {
                    movimiento.setCo2((int) (movimiento.getImporte() * 75)); //añadir parámetro categoria
                } else if (movimiento.getCategoria().equalsIgnoreCase("Energia")) {
                    movimiento.setCo2((int) (movimiento.getImporte() * 25)); //añadir parámetro categoria
                } else if (movimiento.getCategoria().equalsIgnoreCase("alimentacion")) {
                    movimiento.setCo2((int) (movimiento.getImporte() * 10)); //añadir parámetro categoria
                }
            }
        }
        return movimientoRepository.save(movimiento);
    }

    //Delete
    public boolean delete(MovimientoModel movimiento){
        try{
            movimientoRepository.delete(movimiento);
            return true;
        }catch(Exception e){
            return false;
        }
    }

    //Delete
    public boolean deleteById(Integer id){
        try{
            movimientoRepository.deleteById(id);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
