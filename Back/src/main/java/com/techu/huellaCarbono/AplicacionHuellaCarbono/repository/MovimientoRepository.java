package com.techu.huellaCarbono.AplicacionHuellaCarbono.repository;

import com.techu.huellaCarbono.AplicacionHuellaCarbono.model.MovimientoModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovimientoRepository extends MongoRepository<MovimientoModel,Integer> {
}
