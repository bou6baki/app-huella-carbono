package com.techu.huellaCarbono.AplicacionHuellaCarbono.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="movimientos") public class MovimientoModel {

    @Id
    private Integer id;

    @NotNull
    private String descripcion;

    @NotNull
    private Double importe;

    @NotNull
    private String categoria;

    private int co2;

    public MovimientoModel() {}

    // Constructor sin C02 de entrada y cálculo automático según la categoría
    public MovimientoModel(Integer id, String descripcion, Double importe, String categoria) {
        this.id = id;
        this.descripcion = descripcion;
        this.importe = importe;
        if(categoria.equalsIgnoreCase("viaje")){
            this.co2 = (int) (importe*100);
        }else if(categoria.equalsIgnoreCase("comercio")){
            this.co2 = (int) (importe*25);
        }else if(categoria.equalsIgnoreCase("transporte")){
            this.co2 = (int) (importe*75);
        }else if(categoria.equalsIgnoreCase("energia")){
            this.co2 = (int) (importe*25);
        }else if(categoria.equalsIgnoreCase("alimentacion")) {
            this.co2 = (int) (importe*10);
        }
        else{
            this.co2 = (int) (importe*1.5);
        }

        this.categoria=categoria;
    }

    // Constructor con CO2 de entrada
    public MovimientoModel(Integer id, String descripcion, Double importe, String categoria, int co2) {
        this.id = id;
        this.descripcion = descripcion;
        this.importe = importe;
        this.co2 = co2;
        this.categoria = categoria;
    }

    //Métodos GET y SET de cada atributo de la clase

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getImporte() {
        return importe;
    }

    public void setImporte(Double importe) {
        this.importe = importe;
    }
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public int getCo2() {
        return co2;
    }

    public void setCo2(int co2) {
        this.co2 = co2;
    }


}
