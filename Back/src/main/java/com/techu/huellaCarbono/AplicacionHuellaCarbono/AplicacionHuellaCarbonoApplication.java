package com.techu.huellaCarbono.AplicacionHuellaCarbono;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicacionHuellaCarbonoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplicacionHuellaCarbonoApplication.class, args);
	}

}
