package com.techu.huellaCarbono.AplicacionHuellaCarbono.controller;

import com.techu.huellaCarbono.AplicacionHuellaCarbono.model.MovimientoModel;
import com.techu.huellaCarbono.AplicacionHuellaCarbono.service.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/techu/huellaCarbono/v1")
@CrossOrigin (origins = "*", methods = {RequestMethod.GET, RequestMethod.POST,
        RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
public class MovimientoController {

    @Autowired
    MovimientoService movimientoService;

    // Métodos controller
    @GetMapping("/movimientos")
    public List<MovimientoModel> getMovimientos() {
        return movimientoService.findAll();
    }
    @GetMapping("/movimientosCO")
    public int getMovimientosCO() {
        List <MovimientoModel> lista =  movimientoService.findAll();
        int co=0;
        for (int i=0;i<lista.size();i++) {
            co=co+lista.get(i).getCo2();
        } return co;
    }
    @GetMapping("/movimientos/{id}" )
    public Optional<MovimientoModel> getMovimientoId(@PathVariable Integer id){
        return movimientoService.findById(id);
    }

    @PostMapping("/movimientos")
    public MovimientoModel postMovimientos(@RequestBody MovimientoModel newMovimiento){
        movimientoService.save(newMovimiento);
        return newMovimiento;
    }

    @PutMapping("/movimientos")
    public void putMovimientos(@RequestBody MovimientoModel movimientoToUpdate){
        movimientoService.save(movimientoToUpdate);
    }


    @DeleteMapping("/movimientos")
    public boolean deleteMovimientos(@RequestBody MovimientoModel movimientoToDelete){
        if(movimientoService.findById(movimientoToDelete.getId()).isPresent()){
            movimientoService.delete(movimientoToDelete);
            return true;
        }
        return false;
    }

    @DeleteMapping("/movimientos/{id}")
    public boolean deleteMovimientos(@PathVariable Integer id) {
        if (movimientoService.findById(id).isPresent()) {
            movimientoService.deleteById(id);
            return true;
        }
        return false;
    }

    @PatchMapping("/movimientos/{co2}")
    public boolean patchMovimientos(@RequestBody MovimientoModel movimientoToPatch, @PathVariable Integer co2){
        if(movimientoService.findById(movimientoToPatch.getId()).isPresent()){
            movimientoToPatch.setCo2(co2);
            movimientoService.save(movimientoToPatch);
            return true;
        }
        else{
            return false;
        }
    }


}
