import { LitElement, html} from 'lit-element';

class HuellaSidebar extends LitElement {
    
    static get properties(){
        return{
            footsprintTotal: {type: Number}
        };
    }
    
    constructor(){
        super();
        this.footsprintTotal = 0;
    }
    


    render () {
        return html`
            
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <aside>
                <section>   
                    <br></br>                 
                    <div class="mt-5">
                        <button @click="${this.calculaFootsprint}" class="w-100 btn btn-success"><strong>Calcular</strong></button>
                    </div> 
                    <br></br>  
                    <h4 class="text-center">
                        Tu huella de carbono total acumulada es de  <span class="badge badge-pill badge-primary">${this.footsprintTotal}</span>  tCO2e
                    </h4>  
                </section>
            </aside>
        `;

    }

    //Funcion generadora de Evento Calcular Huella Total
    calculaFootsprint(e) {
        console.log("calculaFootsprint en huella-sidebar");
        console.log(this.footsprintTotal);
        //this.footsprintTotal="299";
        
            console.log("getC02");
            console.log("Obteniendo datos de CO2...");
    
            let xhr = new XMLHttpRequest();
    
            xhr.onload = () => {
                 if (xhr.status === 200) {
                     console.log("Peticion completada correctamente");
    
                     let APIResponse = JSON.parse(xhr.responseText);
                     console.log(APIResponse);
                     this.footsprintTotal = APIResponse;                 
                     
                 }           
            }
    
            xhr.open("GET","http://localhost:8081/techu/huellaCarbono/v1/movimientosCO");
            xhr.send();
    
          
        console.log(this.footsprintTotal);
  
        //Generamos el evento de calculaFootsprint
        this.dispatchEvent(new CustomEvent("calculaFootsprint", {})); 
    }

   
}

customElements.define('huella-sidebar', HuellaSidebar)