import { LitElement, html} from 'lit-element';

class HuellaFooter extends LitElement {
    
    static get properties(){
        return{

        };
    }
    
    constructor(){
        super();
    }
    
    
    render () {
        return html`
            <h5>@Huella de carbono App 2020</h5>
        `;

    }
}

customElements.define('huella-footer', HuellaFooter)