import { LitElement, html} from 'lit-element';
import '../ficha-movimiento/ficha-movimiento.js';
import '../huella-form/huella-form.js';
import '../huella-api/huella-api.js';



class HuellaMain extends LitElement {
    
    
    static get properties() {
        return {	
            movimientos: {type: Array},		
            showMoveForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.movimientos = [];
        this.getMovimientos();
        console.log("Movimientos en el constructor de huella-main");
        
        console.log(this.movimientos);
        
        
         //Array de movimientos
         this.movimientos = [
            {
                id: 1,
                name: "Compra Amazon",
                amount: 30.50,
                type: "Comercio",
                footprint: 2               
            }, {
                id: 2,
                name: "Compra Carrefour",
                amount: 122.90,
                type: "Comercio",
                footprint: 4                  
            }, {
                id: 3,
                name: "Compra ElCorteIngles",
                amount: 49.99,
                type: "Comercio",
                footprint: 3  
            }, {
                id: 4,
                name: "Vuelos Iberia",
                amount: 450,
                type: "Transporte",
                footprint: 40
            }, {
                id: 5,
                name: "Billetes Ave",
                amount: 210,
                type: "Transporte",
                footprint: 20  
            }, {
                id: 6,
                name: "Carburante Repsol",
                amount: 55,
                type: "Energia",
                footprint: 20  
            }, {
                id: 7,
                name: "Hotel Booking",
                amount: 250,
                type: "Viaje",
                footprint: 9  
            }, {
                id: 8,
                name: "Recibo agua",
                amount: 53.25,
                type: "Energia",
                footprint: 2  
            }, {
                id: 9,
                name: "Recibo Luz",
                amount: 73.50,
                type: "Energia",
                footprint: 12  
            }
                       
        ];
        // usamos esta propiedad para guardar el estado si monstramos o no la ficha del movimiento
        this.showMoveForm = false;
        console.log("constructor huella main. ShowMoveForm = false");
    }

 
    
    render () {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3 class="text-left offset-1">Listado de movimientos</h3>
            <br></br>
            <div class="row offset-1" id="moveList">
                 <div class="row row-cols-1">
                    ${this.movimientos.map(
                        move =>  html`<ficha-movimiento                                             
                                            @info-movimiento="${this.infoMovimiento}"
                                            id="${move.id}" 
                                            name="${move.descripcion}"
                                            amount="${move.importe}"
                                            type="${move.categoria}"
                                            footprint="${move.co2}"
                                        >
                                        </ficha-movimiento>`
                        
                    )}
                </div>
            </div>
            
            <br></br>

            <div class="row">
            <huella-form id="moveForm" class="d-none border rounded border-primary">
            </huella-form>
            </div>
         `;

    }

    getMovimientos() {
        console.log("getMovimientos");
        console.log("Obteniendo datos de los movimientos...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
             if (xhr.status === 200) {
                 console.log("Peticion completada correctamente");

                 let APIResponse = JSON.parse(xhr.responseText);
                 console.log(APIResponse);
                 this.movimientos = APIResponse;                 
                 
             }           
        }

        xhr.open("GET"," http://localhost:8081/techu/huellaCarbono/v1/movimientos");
        xhr.send();

        console.log("movimientos recuperados en huella-main");                 
        console.log(this.movimientos);

    }
  
  
    infoMovimiento (e) {
        console.log("infoMovimiento en huella-main");
        console.log("Se ha pedido información del movimiento: " + e.detail.id);

       let chosenMove = this.movimientos.filter(
           move => move.id === e.detail.id
        );
         console.log(chosenMove);
         
        //aca pasamos los datos de la pesona seleccionada al formulario, al objeto dentro del formulario
        this.shadowRoot.getElementById("moveForm").move = chosenMove[0];
        this.shadowRoot.getElementById("moveForm").editingMove = true;
        console.log(chosenMove[0]);
        this.showMoveForm = true;
        
    } 

    updated(changedProperties) {
        console.log("updated");
        if(changedProperties.has("showMoveForm")) {
            console.log("Ha cambiado el valor de la propiedad showMoveForm en huella-main");
            if(this.showMoveForm === true) {
                this.showMoveFormData();
            } else {
                this.showMoveList();
            }
        } 

        if(changedProperties.has("movimientos")) {
            console.log("Ha cambiado el valor de la propiedad movimientos en huella-main");
           
            this.dispatchEvent(
                new CustomEvent("updated-movimientos", {
                    detail: { movimientos: this.people }
                }
                )
            );
        }

    }

    showMoveFormData() {
        console.log("showMoveFormData");
        console.log("Mostrando ficha de movimiento");
        this.shadowRoot.getElementById("moveForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("moveList").classList.add("d-none");
    }

    showMoveList() {
        console.log("showMoveList");
        console.log("Mostrando listado de moviemientos");
        this.shadowRoot.getElementById("moveList").classList.remove("d-none");
        this.shadowRoot.getElementById("moveForm").classList.add("d-none");	
    }




    

}

customElements.define('huella-main', HuellaMain)