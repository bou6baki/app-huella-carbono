import { LitElement, html} from 'lit-element';

class HuellaHeader extends LitElement {
    
    static get properties(){
        return{

        };
    }
    
    constructor(){
        super();
    }
    
    
    render () {
        return html`
            <h1>Huella de carbono App</h1> 
            <br></br>
        `;

    }
}

customElements.define('huella-header', HuellaHeader)