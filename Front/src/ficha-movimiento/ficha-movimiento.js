import { LitElement, html} from 'lit-element';

class FichaMovimiento extends LitElement {
    
    static get properties(){
        return{

            id: {type: Number},
            name: {type: String},
            amount: {type: Number},
            type: {type: String},
            footprint:  {type: Number},        

        };
    }

    
    constructor(){
        super();
    }
    
    
    render () {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div class="card h-5">   
                <div class="row row-cols-1 row-cols-sm-6">              
                    <h5 class="card-title col-1">${this.name}</h5> 
                    <h6 class="card-title col-2">${this.amount} €</h6>
                    <h6 class="card-title col-3">${this.type}</h6>
                    <h6 class="card-title col-4">${this.footprint} tCO2e</h6> 
                    <button @click="${this.deleteMov}"class="btn btn-danger col-5"><strong>X</strong></button>
                    <button @click="${this.moreInfo}"class="btn btn-info col-6"><strong>Info</strong></button>                
                </div>          
            </div>
        `;

    }

    moreInfo (e){
        console.log("moreInfo");
        console.log("Se ha pedido info del movimiento " + this.name + ", con id:" + this.id);
        
        this.dispatchEvent(
            new CustomEvent(
                "info-movimiento",
                {
                    detail: {
                        id: this.id
                    }
                }
            )
        );
    } 



}

customElements.define('ficha-movimiento', FichaMovimiento)