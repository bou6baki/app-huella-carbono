import { LitElement, html} from 'lit-element';

 
class HuellaForm extends LitElement {
    static get properties() {
        return {
         move: {type: Array},
         editingMove: {type: Boolean}
        }
    }

    constructor() {
        super();
        this.resetFormData();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
						<label>ID</label>
						<input type="text" @input="${this.updateId}" .value="${this.move.id}" ?disabled="${this.editingMove}" id="moveFormName" class="form-control" placeholder="ID del movimiento"/>
                    </div>
                    <div class="form-group">
						<label>Descripcion</label>
						<input type="text" @input="${this.updateName}" .value="${this.move.name}" ?disabled="${this.editingMove}" id="moveFormName" class="form-control" placeholder="Descripción del movimiento"/>
					</div>
					<div class="form-group">
						<label>Categoría</label>
						<textarea @input="${this.updateType}" .value="${this.move.type}" class="form-control" placeholder="Cateogoria del movimiento" rows="5"></textarea>
					</div>
					<div class="form-group">
						<label>Importe (€)</label>
						<input type="text" @input="${this.updateAmount}" .value="${this.move.amount}" class="form-control" placeholder="Importe"/>
                    </div>
                    <div class="form-group">
						<label>Huella de carbono (tCO2e)</label>
						<input type="text" @input="${this.updateFootprint}" .value="${this.move.footprint}" class="form-control" placeholder="footprint"/>
					</div>
					<button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
			        <button @click="${this.storeMove}" class="btn btn-success"><strong>Guardar</strong></button>
                   
                </form>
            </div>


        `;
    }

    resetFormData() {

        console.log("Reset DataForm");
      
        this.move = {};
        this.move.id = 0;
        this.move.name = "";
        this.move.amount = 0;
        this.move.type = "";
        this.move.footprint = 0;

        this.editingMove = false;

    }

    //Databinding de la propiedad id
    updateId(e) {
        console.log("updateId");
		console.log("Actualizando la propiedad Id con el valor " + e.target.value);
        this.move.id = e.target.value;
    }

     //Databinding de la propiedad name
     updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.move.name = e.target.value;

    }

     //Databinding de la propiedad amount
     updateAmount(e) {
        console.log("updateAmount");
		console.log("Actualizando la propiedad amount con el valor " + e.target.value);
		this.move.amount = e.target.value;
    }


    //Databinding de la propiedad type
    updateType(e) {
        console.log("updateType");
		console.log("Actualizando la propiedad type con el valor " + e.target.value);
		this.move.type = e.target.value;
    }

    //Databinding de la propiedad footprint
    updateFootprint(e) {
        console.log("updateFootprint");
		console.log("Actualizando la propiedad footprint con el valor " + e.target.value);
		this.move.footprint = e.target.value;
    }

}

customElements.define('huella-form', HuellaForm)