import { LitElement, html} from 'lit-element';
import '../huella-header/huella-header.js';
import '../huella-footer/huella-footer.js';
import '../huella-main/huella-main.js';
import '../huella-sidebar/huella-sidebar.js';
import '../huella-api/huella-api.js';




class HuellaApp extends LitElement {
    
    static get properties(){
        return{
           
        };
    }
    
    constructor(){
        super();
    }
    
   
    render () {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <huella-header></huella-header>
            <div class="row">
                <huella-main class="col-8"></huella-main>
                <huella-sidebar class=" offset-1 col-2"></huella-sidebar>                
            </div>
                            
            <huella-footer></huella-footer>
            
                         
        `;

    }

   
}

{/* <div class="row"> 
                <huella-api class="offset-1">Prueba API HUELLA</huella-api> 
            </div>   */}

customElements.define('huella-app', HuellaApp)