import { LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {
    
    
    static get properties() {
        return {			
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();
        
        //People array
        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 3,
                photo: {
                    "src": "./img/persona.png",
                    "alt": "Ellen Ripley"
                },
                profile: "Lorem ipsum dolor sit amet."
            }, {
                name: "Bruce Banner",
                yearsInCompany: 5,
                photo: {
                    "src": "./img/persona.png",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit."
            }, {
                name: "Éowyn",
                yearsInCompany:10,
                photo: {
                    "src": "./img/persona.png",
                    "alt": "Éowyn"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            }, {
                name: "Turanga Leela",
                yearsInCompany:9,
                photo: {
                    "src": "./img/persona.png",
                    "alt": "Turanga Leela"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua."
            }, {
                name: "Tyrion Lannister",
                yearsInCompany:10,
                photo: {
                    "src": "./img/persona.png",
                    "alt": "Tyrion Lannister"
                },
                profile: "Lorem ipsum dolor sit amet."
            }
        ];
        
        //Inicializacion  para NO mostrar formulario persona por defecto
        this.showPersonForm = false;
    }
    
    
    render () {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person =>  html`<persona-ficha-listado 
                                            @delete-person="${this.deletePerson}"
                                            @info-person="${this.infoPerson}"
                                            fname="${person.name}" 
                                            yearsInCompany="${person.yearsInCompany}"
                                            profile="${person.profile}"
                                            .photo="${person.photo}"
                                        >
                                        </persona-ficha-listado>`
                        
                    )}
                </div>
            </div>

            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary" 
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}">
                </persona-form>
            </div>
        `;

    }

    // //Funcion generadora de Evento persona-sidebar
    // updatedPeople(e) {
    //     console.log("updatedPeople en persona-main");
    //     console.log("Se va a actualizar el array de personas");
  
    //     //Generamos el evento de crear nueva persona
    //     this.dispatchEvent(new CustomEvent("updated-people", {})); 
    // }

    deletePerson(e) {
        console.log("deletePerson en persona-main");

        console.log("Me piden borrar la persona " + e.detail.name );
        console.log("Personas antes de borrar ")
        console.log(this.people);

        this.people = this.people.filter(
          person => person.name != e.detail.name            
        );

        console.log("Personas despues de borrar ")
        console.log(this.people);
        
    }

    infoPerson(e) {
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido información de la persona " + e.detail.name); 

        let chosePerson = this.people.filter(
            person => person.name === e.detail.name
        );

        //console.log(chosePerson);

        this.shadowRoot.getElementById("personForm").person = chosePerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;

    }

    updated(changedProperties) {
        console.log("updated");
        if(changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if(this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        } 

        if(changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");
           
            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    detail: { people: this.people }
                }
                )
            );
        }

    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
    }

    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona:" + e.detail.person);
        //console.log(e.detail.person);

        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre" + e.detail.person.name);

            //Para actualizar el array people, voy siguiendo los elementos del array y cuando coincide con el person del evento, lo actualizo, y cuando no, simplemente lo dejo
            this.people = this.people.map(
                person => person.name === e.detail.person.name
                            ? person = e.detail.person : person
            );
            // let indexOfPerson =
            //     this.people.findIndex(
            //         person => person.name === e.detail.person.name
            //     );
            // if(indexOfPerson >= 0) {
            //     console.log("Persona encontrada");
            //     this.people[indexOfPerson] = e.detail.person;
            // }
        } else {
            console.log("Se va a almacenar la persona de nombre" + e.detail.person.name);
            // this.people.push(e.detail.person); -> Esto hace lo mismo que la linea de abajo, pero sin cambiar la propiedad people para que lo detecte changeproperties
            this.people = [...this.people, e.detail.person];
            console.log("Persona almacenada");
        }

        
        console.log("Proceso terminado");
       
        this.showPersonForm = false;
    
    }

    

}

customElements.define('persona-main', PersonaMain)