import { LitElement, html, css} from 'lit-element';

 class TestBootstrap extends LitElement {

    static get styles(){
        return html`
            .redbg {
                backgorund-color: red;
            }
            .greenbg {
                backgroud-color: green;
            }
            .bluebg {
                background-color: blue;
            }
            .greybg {
                blackground-color: grey;
            }
        `;
    }

    render() {

        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3>test bootstrap!</h3>
        `;

    }

}

 

customElements.define('test-bootstrap', TestBootstrap)