import { LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {
    
    static get properties(){
        return{
            person: {type: Object},
            editingPerson: {type: Boolean}

        };
    }
    
    constructor(){
        super();
        this.resetFormData();       

    }
    


    render () {
        return html`
            
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <div>
				<form>
					<div class="form-group">
						<label>Nombre Completo</label>
						<input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" id="personFormName" class="form-control" placeholder="Nombre Completo"/>
					<div>
					<div class="form-group">
						<label>Perfil</label>
						<textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
					<div>
					<div class="form-group">
						<label>Años en la empresa</label>
						<input type="text" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa"/>
					<div>
					<button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
			        <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
				</form>
			</div>
        `;

    }


    resetFormData() {
        console.log("resetFormData");
        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }

    goBack(e) {
        console.log("goBack");	  
        e.preventDefault();	
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
        this.resetFormData();	
    }

    storePerson(e) {
        console.log("storePerson");	  
        e.preventDefault();	

        console.log("La propiedad name vale " + this.person.name);
	    console.log("La propiedad profile vale " + this.person.profile);
        console.log("La propiedad yearsInCompany vale " + this.person.yearsInCompany);	
        

        this.person.photo = {
            "src": "./img/persona.png",
            "alt": "Persona"
        }
       
        this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
                    person: {
                        name: this.person.name,
                        profile: this.person.profile,
                        yearsInCompany: this.person.yearsInCompany,
                        photo: this.person.photo
                    },
                    editingPerson: this.editingPerson
                }
            })
        );	
        this.resetFormData();	
    }


    //Databinding de la propiedad Name
    updateName(e) {
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + e.target.value);
        this.person.name = e.target.value;

    }

    //Databinding de la propiedad Profile
    updateProfile(e) {
        console.log("updateProfile");
		console.log("Actualizando la propiedad profile con el valor " + e.target.value);
		this.person.profile = e.target.value;
    }

    //Databinding de la propiedad yearsInCompany
    updateYearsInCompany(e) {
        console.log("updateYearsInCompany");
		console.log("Actualizando la propiedad yearsInCompany con el valor " + e.target.value);
		this.person.yearsInCompany = e.target.value;
    }



}

customElements.define('persona-form', PersonaForm)