import { LitElement, html} from 'lit-element';

 

class HuellaApi extends LitElement {

    static get properties() {
        return {
            movimientos: {type: Array}
        }
    }

    constructor () {
        super();

        this.movimientos = [];
        this.getMovimientos();
        
    
    }
    
    render() {
        return html`
        
        <div>Esto es una prueba del GET del Api Huella de Carbono entre Front y back:</div>
        
            ${this.movimientos.map(
                movimiento =>
                html `<div>El movimiento ${movimiento.id}: "${movimiento.descripcion}", por importe ${movimiento.importe} €, de categoria ${movimiento.categoria} tiene una huella de carbono de ${movimiento.co2}.</div>`
            
            )}

        
        `;

    }


    getMovimientos() {
        console.log("getMovimientos");
        console.log("Obteniendo datos de los movimientos...");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
             if (xhr.status === 200) {
                 console.log("Peticion completada correctamente");

                 let APIResponse = JSON.parse(xhr.responseText);
                 console.log(APIResponse);
                 this.movimientos = APIResponse;                 
                 console.log("movimientos recuperados en huella-api");                 
                 console.log(this.movimientos);
             }           
        }

        xhr.open("GET"," http://localhost:8081/techu/huellaCarbono/v1/movimientos");
        xhr.send();

    }

}

 

customElements.define('huella-api', HuellaApi)